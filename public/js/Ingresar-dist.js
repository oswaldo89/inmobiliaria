var vue = new Vue({
    el: '#loginApp',
    data: {
        model: {
          email: '',
          password: '',
          csrf: ''
        }
    },
    methods: {
      signin: function(){
        var self = this;
        this.loading(true);
        axios.post('/login', self.model).then(function(response) {
            self.loading(false);
            if(response.data.status == true){
              window.location = ADMIN_PATH;
            }else{
              switch (response.data.code) {
                case 400:
                  swal({ title: 'Registro de usuario', text: response.data.error, timer: 10000 });
                  break;
                case 401:
                  swal({ title: 'Activación de usuario', text: response.data.error, timer: 10000 });
                  break;
                case 402:
                  //self.model.email = "";
                  //self.model.password = "";
                  swal({ title: 'Credenciales invalidas', text: response.data.error, timer: 10000 });
                  break;
                  
              }
            }
        }).catch(function(error) {
            //console.log(error);
            self.loading(false);
        });
      },
      loading: function(value){
        if(value){
          NProgress.start();
        }else{
          NProgress.done();
        }
      }
    }
});