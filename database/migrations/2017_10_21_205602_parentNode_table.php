<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParentNodeTable extends Migration
{
    public function up()
    {
        Schema::create('parent_nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('name');
            $table->longText('telephone')->nullable();
            $table->longText('email')->nullable();
            $table->longText('img')->nullable();
            $table->string('geolocation');
            $table->string('paisId');
            $table->string('estadoId');
            $table->string('municipioId');
            $table->boolean('isActive')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('parent_nodes');
    }
}
