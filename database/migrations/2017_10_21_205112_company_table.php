<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyTable extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ceoId');
            $table->longText('logo')->nullable();
            $table->string('companyName');
            $table->string('telephone')->nullable();
            $table->string('address')->nullable();
            $table->string('paisId');
            $table->string('estadoId');
            $table->string('municipioId');
            $table->boolean('isActive')->default(true);
            $table->boolean('isVerifiedAccount')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
