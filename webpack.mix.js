const { mix } = require('laravel-mix');

mix.scripts([
'resources/assets/js/libs/modernizr-2.8.1.min.js',
'resources/assets/js/libs/jquery.min.js',
'resources/assets/js/libs/vue.js',
'resources/assets/js/libs/v-mask.min.js',
'resources/assets/js/libs/axios.js',
'resources/assets/js/libs/nprogress.js',
'resources/assets/js/libs/sweetalert2.min.js',
'resources/assets/js/libs/v-money.js'
], 'public/js/app.js');


mix.scripts([
'resources/assets/template/js/common.js',
'resources/assets/template/js/jquery.prettyPhoto.js',
'resources/assets/template/js/owl.carousel.min.js',
'resources/assets/template/js/variables.js',
'resources/assets/template/js/scripts.js',
'resources/assets/template/js/agencies.js',
'resources/assets/js/libs/jquery.cropit.js'
], 'public/js/template.js');

mix.styles([
'resources/assets/template/css/bootstrap.css',
'resources/assets/template/css/style.css',
'resources/assets/css/sweetalert2.min.css',
'resources/assets/css/style.css',  
'resources/assets/css/nprogress.css'
], 'public/css/style.css');

// DataTable
mix.scripts([
  'resources/assets/js/libs/datatable/jquery.dataTables.min.js',
  'resources/assets/js/libs/datatable/dataTables.bootstrap.min.js',
  'resources/assets/js/libs/datatable/dataTables.responsive.min.js',
  'resources/assets/js/libs/datatable/responsive.bootstrap.min.js'
], 'public/js/datatable.js');

mix.styles([
  'resources/assets/css/libs/datatable/dataTables.bootstrap.min.css',
  'resources/assets/css/libs/datatable/responsive.bootstrap.min.css'
], 'public/css/datatable.css');

mix.scripts('resources/assets/js/controllers/Registro-controller.js', 'public/js/Registro-dist.js');
mix.scripts('resources/assets/js/controllers/Ingresar-controller.js', 'public/js/Ingresar-dist.js');
mix.scripts('resources/assets/js/controllers/DesarrollosList-controller.js', 'public/js/DesarrollosList-dist.js');
mix.scripts('resources/assets/js/controllers/DesarrollosNuevo-controller.js', 'public/js/DesarrollosNuevo-dist.js');
mix.scripts('resources/assets/js/controllers/DesarrollosModelList-controller.js', 'public/js/DesarrollosModelList-dist.js');
