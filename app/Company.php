<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  protected $fillable = [
      'ceoId',
      'logo',
      'companyName',
      'paisId',
      'estadoId',
      'municipioId',
      'telephone',
      'address',
      'isActive',
      'isVerifiedAccount'
  ];
}
