<?php
/**
 * Created by IntelliJ IDEA.
 * User: oswaldogh89
 * Date: 21/10/17
 * Time: 15:59
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentNode extends Model
{
    protected $fillable = [
        'company_id',
        'name',
        'telephone',
        'email',
        'img',
        'geolocation',
        'paisId',
        'estadoId',
        'municipioId',
        'isActive'
    ];
}