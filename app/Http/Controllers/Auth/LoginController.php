<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function login(Request $request)
    {
        $activo = 0;
        $email = $request->get($this->username());
        $client = User::where($this->username(), $email)->first();
        
        if(!$client){
          return response()->json([
               'status' => false,
               'code' => 400,
               'error' => 'El usuario no esta registrado ni asociado a ninguna Desarrolladora Inmobiliaria.'
          ]);
        }
        
        if($client->confirmed == 0){
          return response()->json([
               'status' => false,
               'code' => 401,
               'error' => 'El usuario esta inactivo, favor de buscar el correo de confirmación y asi activar su cuenta.'
          ]);
        }
      
        $this->validateLogin($request);
                
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        
        return $this->sendFailedLoginResponse($request);
    }
    
    
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

       return response()->json([
            'status' => true,
            'code' => 200,
            'user' => $this->guard()->user(),
       ]);
    }
    
    protected function sendFailedLoginResponse(Request $request)
    {
      return response()->json([
          'status' => false,
          'code' => 402,
          'error' => 'El correo y/o contraseña son incorrectos, verifique y vuelva a intentarlo',
      ]);
    }
}
