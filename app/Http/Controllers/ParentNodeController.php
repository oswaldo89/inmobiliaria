<?php

namespace App\Http\Controllers;

use App\Company;
use App\ParentNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ParentNodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::where('ceoId',Auth::user()->id)->first();
        return response()->json(ParentNode::where("company_id",$company->id)->orderBy('id', 'DESC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.DesarrollosNuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result['status'] = false;
        $result['message'] = null;
        $company = Company::where('ceoId',Auth::user()->id)->first();

        $parent = new ParentNode($request->all());
        $parent->company_id = $company->id;

        if ($parent->save()){
            $result['status'] = true;
            $result['message'] = 'Agregado correctamente.';
        }else{
            $result['status'] = false;
            $result['message'] = 'Ocurrio un error, no se pudo guardar.';
        }

        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.DesarrollosModelList');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result['status'] = false;
        $result['message'] = null;
        $item = ParentNode::find($id);
        if($item->delete()){
            $result['status'] = true;
            $result['message'] = 'El registro ha sido eliminado correctamente.';
        }else{
            $result['status'] = false;
            $result['message'] = 'Ocurrio un error inesperado.';
        }
        return response()->json($result);
    }
}
