<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
      $existUser = User::where('email', '=', $request->input('email'))->first();
      
      $result['status'] = false;
      $result['message'] = null;
      
      if(count($existUser) == 0){
        $user = new User($request->all());
        $user->confirmation_code = str_random(30);
        $user->password = Hash::make($request->input('password'));

        try {
          if($user->save()){
            $realState = new Company($request->all());
            $realState->ceoId = $user->id;

            if($realState->save()){
              $result['status'] = true;
              $result['message'] = 'Agregado correctamente, verifique su correo electronico para activar la cuenta';
              Mail::to($user->email)->send(new WelcomeMail($user));
            }else{
              $result['message'] = 'No se pudo Guardar la Desarrolladora Inmobiliaria';
            }

          }else{
            $result['message'] = 'No se pudo Guardar el usuario';
          }

        } catch (QueryException $e) {
          if(config('app.debug')){
            $result['message'] = $e->getMessage();
          }
          else {
            $result['message'] = "Ocurrio un error inesperado.";
          }
        }
      }else{
        $result['status'] = false;
        if($existUser->confirmed){
          $result['message'] = 'El correo ya se encuentra ligado a una Desarrolladora Inmobiliaria, si olvido su contraseña:
                                <a href="/resetPassword">CLICK AQUI.!</a>';
        }else{
          $result['message'] = 'El correo ya se encuentra registrado en nuestro sistema, pero no ha sido activado , si deseas reenviar el correo de activación:
                                <a href="/resendActivation">CLICK AQUI.!</a>';
        }
        
      }
      
      return response()->json($result);
    }
}
