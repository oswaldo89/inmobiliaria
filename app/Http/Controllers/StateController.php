<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
    public function show($id_pais)
    {
        return json_encode(State::where("id_pais",$id_pais)->get());
    }
}
