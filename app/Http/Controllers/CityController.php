<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
  public function show($id_estado)
  {
      return json_encode(City::where("cve_ent",$id_estado)->get());
  }
}
