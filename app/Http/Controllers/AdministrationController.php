<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class AdministrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      return view('pages.administration');
    }

}
