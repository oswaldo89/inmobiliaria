@extends('layouts.default')

@section('content')
  <!-- INCLUDES/SEARCH-->
  <section id="home-search-section" data-stellar-background-ratio="0.5">
      @include('includes.search')
  </section>
  <!-- INCLUDES/SEARCH-->

  <!-- INCLUDES/ACTIONBOX -->
  <div class="action-box">
      @include('includes.actionbox')
  </div>
  <!-- INCLUDES/ACTIONBOX -->

  <!-- INCLUDES/SLIDER-->
  <div class="parallax pattern-bg" data-stellar-background-ratio="0.5">
      @include('includes.sliderwraper')
  </div>
  <!-- INCLUDES/SLIDER-->

  <!-- INCLUDES/CONTENT -->
  <div class="content colored">
      @include('includes.contentgraper')
  </div>
  <!-- INCLUDES/CONTENT -->

  <!-- INCLUDES/PARTNERS -->
  <div class="parallax pattern-bg" data-stellar-background-ratio="0.5">
      @include('includes.partnerswrapper')
  </div>
  <!-- INCLUDES/PARTNERS -->
@stop
