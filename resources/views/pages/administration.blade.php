@extends('layouts.default') @section('content')



<div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Administrar</h1>
            </div>
        </div>
    </div>
</div>

<div class="content" id="administration">
    <div class="container">

        <div class="row" style="margin:20px;">
            
          <div class="tabbable-panel">
              <div class="tabbable-line">
                  <ul class="nav nav-tabs ">
                      <li class="active"> <a href="#tab_default_1" data-toggle="tab"><i class="fa fa-building-o" aria-hidden="true"></i> Desarrollos</a> </li>
                      <li> <a href="#tab_default_2" data-toggle="tab"><i class="fa fa-bar-chart" aria-hidden="true"></i> Estadisticas y Graficas</a> </li>
                      <li> <a href="#tab_default_3" data-toggle="tab"><i class="fa fa-envelope-o" aria-hidden="true"></i> Mensajes</a> </li>
                      <li> <a href="#tab_default_4" data-toggle="tab"><i class="fa fa-cog" aria-hidden="true"></i> Configuración de cuenta</a> </li>
                  </ul>
                  <div class="tab-content">
                      <div class="tab-pane active" id="tab_default_1" style="overflow: auto;min-height: 540px;">
                        
                        @include('admin.DesarrollosList')
                        
                      </div>
                      <div class="tab-pane" id="tab_default_2" style="overflow: auto;min-height: 540px;">
                          GRAFICAS
                      </div>
                      <div class="tab-pane" id="tab_default_3" style="overflow: auto;min-height: 540px;">
                          MENSAJES
                      </div>
                      <div class="tab-pane" id="tab_default_4" style="overflow: auto;min-height: 540px;">
                          CONFIGURACION
                      </div>
                  </div>
              </div>
          </div>
            
        </div>
    </div>
</div>



@stop