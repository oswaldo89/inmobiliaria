@extends('layouts.default') @section('content')

<div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Registrar</h1>
            </div>
        </div>
    </div>
</div>

<div class="content" id="register">
    <div class="container">

        <div class="row">
            <!-- BEGIN MAIN CONTENT -->
            <div class="main col-sm-12">

                <div class="col-sm-6">

                    <div class="panel panel-default box-shadow">
                        <div class="panel-heading">
                            <h3 class="panel-title">Datos de la Desarrolladora Inmobiliaria</h3>
                        </div>
                        <div class="panel-body">

                            <div style="text-align:center;">
                                <small>La imagen que seleccione aqui, sera la que se muestre como el logo de la desarrolladora, la imagen debe ser de <strong>350 x 200 pixeles</strong> o mayor a proporción.</small>
                            </div>

                            <div id="image-cropper" style="width:100%; margin-top:10px;">
                                <div class="cropit-preview"></div>
                                <input type="range" class="cropit-image-zoom-input" style="width: 55%; margin-left: 22%;" />
                                <input type="file" class="cropit-image-input" ref="inputImageFile" />
                            </div>
                            
                            <div style="text-align: center; margin-top:-15px; margin-bottom: 20px;">
                                <button type="buton" class="btn btn-fullcolor select-image-btn" v-on:click="selectImage()">Selecciona Imagen</button>
                            </div>

                            <div class="col-lg-6">
                                <strong>Nombre de la Desarrolladora*</strong>
                                <input type="text" class="form-control" v-model="model.companyName" @input="model.companyName = $event.target.value.toUpperCase()"/>
                            </div>
                            <div class="col-lg-6">
                                <strong>Telefono</strong>
                                <input type="text" v-mask="'(##) ####-########'" class="form-control" v-model="model.telephone" />
                            </div>
                            <div class="col-lg-12">
                                <strong>Dirección</strong>
                                <input type="text" class="form-control" v-model="model.address" @input="model.address = $event.target.value.toUpperCase()"/>
                                
                                
                                <select class="form-control" v-model="country">
                                  <option v-for="item in countries" v-bind:value="item.id_pais">@{{ item.nombre }}</option>
                                </select>
                                
                                <select class="form-control" v-model="state">
                                  <option v-for="item in states" v-bind:value="item.cve_ent">@{{ item.nom_ent }}</option>
                                </select>
                                
                                <select class="form-control" v-model="city">
                                  <option v-for="item in cities" v-bind:value="item.cve_mun">@{{ item.nom_mun }}</option>
                                </select>
                                
                            </div>

                        </div>
                    </div>

                </div>

                <div class="col-sm-6">

                    <div class="panel panel-default box-shadow">
                        <div class="panel-heading">
                            <h3 class="panel-title">Datos del Administrador</h3>
                        </div>
                        <div class="panel-body">
                            <strong>Nombre completo*</strong>
                            <input type="text" class="form-control" v-model="model.ceoName" @input="model.ceoName = $event.target.value.toUpperCase()"/>
                            <strong>Correo electronico*</strong>
                            <input type="email" class="form-control" v-model="model.email" @input="model.email = $event.target.value.toUpperCase()"/>
                            <strong>Contraseña*</strong>
                            <input type="password" class="form-control" v-model="model.password" />
                            <strong>Repetir contraseña*</strong>
                            <input type="password" class="form-control" v-model="model.secondPassword" />
                            
                            <div style="text-align:center; margin-top:20px;margin-bottom:15px;">
                                <small>Al ingresar su tarjeta de crédito, no resibirá cobro alguno, puede probar la plataforma 15 dias <strong>GRATIS</strong>, antes de esa fecha puede cancelar la suscripción en cualquier momento.</small>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="cardNumber">NUMERO DE TARJETA</label>
                                        <div class="input-group">
                                            <input type="tel" class="form-control" name="cardNumber" placeholder="XXXX-XXXX-XXXX-XXXX" autocomplete="cc-number" required autofocus />
                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-7 col-md-7">
                                    <div class="form-group">
                                        <label for="cardExpiry"><span class="hidden-xs">FECHA DE EXPIRACIÓN</label>
                                        <input type="tel" class="form-control" name="cardExpiry" placeholder="MM / YY" autocomplete="cc-exp" required />
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-5 pull-right">
                                    <div class="form-group">
                                        <label for="cardCVC">CV CODIGO</label>
                                        <input type="tel" class="form-control" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" required />
                                    </div>
                                </div>
                            </div>
                            
                            <img src="imgs/cards.png" width="198">

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms">Confirmo que he leido y que acepto los terminos y condiciones</a>.
                                </label>
                            </div>

                            <div style="text-align: center;">
                                <button type="buton" class="btn btn-fullcolor" v-on:click="createAccount" :disabled="disabledFormSubmit">Crear cuenta</button>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <!-- END MAIN CONTENT -->

        </div>
    </div>
</div>

<script src="{{ asset('js/Registro-dist.js') }}"></script>

@stop