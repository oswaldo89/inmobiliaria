@extends('layouts.default')
@section('content')

<div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-title">Verificacion de correo</h1>
      </div>
    </div>
  </div>
</div>

<div class="content" id="app">
  <div class="row">
      <div class="main col-sm-12" style="text-align:center;">
          <h2>{{ $response['user'] }}</h2>
          <h3>{{ $response['message'] }}</h3>
          @if ($response['status'])
              <img src="{{URL::to('/imgs/feliz.png')}}" width="300">
          @else
              <img src="{{URL::to('/imgs/triste.png')}}" width="300">
          @endif
          
      </div>
  </div>
</div>

@stop
