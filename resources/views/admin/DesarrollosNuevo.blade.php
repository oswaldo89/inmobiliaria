@extends('layouts.default')
@section('content')

<div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Nuevo Desarrollo</h1>
            </div>
        </div>
    </div>
</div>

<div class="content" id="app">
    <div class="container">

        <div style="margin:20px;">
            <div class="row" style="margin-bottom: 20px; margin-left: 1px;">

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <input type="checkbox" checked data-toggle="toggle" data-on="Activado" data-off="Desactivado"
                           data-onstyle="success" data-offstyle="danger" data-width="120">
                </div>

                <div class="col-lg-6 col-md-6  col-sm-6">
                    <div class="pull-right" style="margin-right: 13px;">
                        <a href="#" style="margin-bottom: 10px;" class="btn btn-primary" v-on:click="save()" :disabled="disabledFormSubmit"><i
                                    class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</a>
                    </div>
                </div>

            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="overflow: auto;">
                        <i class="fa fa-database" aria-hidden="true"></i> Datos del Desarrollo
                    </div>
                    <div class="panel-body">
                        <label>Nombre*</label>
                        <input class="form-control" type="text" placeholder="Nombre del desarrollo"
                               v-model="model.name" @input="model.name = $event.target.value.toUpperCase()">
                        <label>Teléfono*</label>
                        <input class="form-control" type="text" v-mask="'(##) ####-########'" placeholder="555-555-555-555" v-model="model.telephone">
                        <label>Correo electronico*</label>
                        <input class="form-control" type="text" placeholder="ejemplo@dominio.com" v-model="model.email" @input="model.email = $event.target.value.toUpperCase()">
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" style="overflow: auto;">
                        <i class="fa fa-picture-o" aria-hidden="true"></i> Logo o Banner del Desarrollo (210 x 109 pixeles)
                        <button class="btn btn-primary btn btn-primary btn-xs pull-right"
                                v-on:click="openImagesSelector()"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Agregar
                        </button>
                    </div>
                    <div class="panel-body imgPickerFondo" style="height: 200px;overflow: auto; text-align: center;">
                        <input type='file' id="imgInp" style="display: none;"/>
                        <img id="blah" style="height: 100%;display: none;"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-globe" aria-hidden="true"></i> Ubicación*</div>
                    <div class="panel-body">
                        <div style="text-align: center;">
                            <label>Agrega el marcador haciendo clic en el mapa para seleccionar el lugar
                                deseado.</label>
                        </div>
                        <input id="pac-input" class="controls" type="text" placeholder="Buscar localización exacta...">
                        <div id="map" class="box-shadow" style="margin: 0 auto 0 auto;"></div>
                        <div style="margin-top: 15px; margin-bottom: 75px;">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <select class="form-control" v-model="country">
                                    <option v-for="item in countries" v-bind:value="item.id_pais">@{{ item.nombre }}
                                    </option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <select class="form-control" v-model="state">
                                    <option v-for="item in states" v-bind:value="item.cve_ent">@{{ item.nom_ent }}
                                    </option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <select class="form-control" v-model="city">
                                    <option v-for="item in cities" v-bind:value="item.cve_mun">@{{ item.nom_mun }}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>
</div>

<script src="{{ asset('js/DesarrollosNuevo-dist.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk_HDgT86e3mHfgFSGPLO5T6L3bc2xx7Q&libraries=places&callback=map.initAutocomplete"
        async defer></script>

@stop