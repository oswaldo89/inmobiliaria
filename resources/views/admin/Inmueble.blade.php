@extends('layouts.default') @section('content')

    <style>
        .file_input{
            display: inline-block;
            padding: 10px 16px;
            outline: none;
            cursor: pointer;
            text-decoration: none;
            text-align: center;
            white-space: nowrap;
            font-family: sans-serif;
            font-size: 11px;
            font-weight: bold;
            border-radius: 3px;
            color: #008BFF;
            border: 1px solid #008BFF;
            vertical-align: middle;
            background-color: #fff;
            margin-bottom: 10px;
            box-shadow: 0px 1px 5px rgba(0,0,0,0.05);
            -webkit-transition: all 0.2s;
            -moz-transition: all 0.2s;
            transition: all 0.2s;
        }
        .file_input:hover,
        .file_input:active {
            background: #008BFF;
            color: #fff;
        }
    </style>

    <div class="parallax colored-bg pattern-bg" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-title">Nuevo Desarrollo</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="content" id="app">
        <div class="container">

            <div style="margin:20px;">
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <input type="checkbox" checked data-toggle="toggle" data-on="Activado" data-off="Desactivado" data-onstyle="success" data-offstyle="danger" data-width="120">
                    </div>

                    <div class="col-lg-6 col-md-6  col-sm-6">
                        <div class="priceBox">
                            <money v-model="price" v-bind="money" class="form-control" style="width: 200px;"></money>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-globe" aria-hidden="true"></i> Ubicación</div>
                        <div class="panel-body">
                            <div style="text-align: center;">
                                <label>Agrega el marcador haciendo clic en el mapa para seleccionar el lugar exacto.</label>
                            </div>
                            <input id="pac-input" class="controls" type="text" placeholder="Buscar localización exacta...">
                            <div id="map" class="box-shadow" style="margin: 0 auto 0 auto;"></div>
                            <div style="margin-top: 15px;">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <select class="form-control" v-model="country">
                                        <option v-for="item in countries" v-bind:value="item.id_pais">@{{ item.nombre }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <select class="form-control" v-model="state">
                                        <option v-for="item in states" v-bind:value="item.cve_ent">@{{ item.nom_ent }}</option>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <select class="form-control" v-model="city">
                                        <option v-for="item in cities" v-bind:value="item.cve_mun">@{{ item.nom_mun }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="overflow: auto;">
                            <i class="fa fa-files-o" aria-hidden="true"></i> Imagen ( Principal )
                            <button class="btn btn-primary btn btn-primary btn-xs pull-right" v-on:click="openImagesSelector()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar</button>
                        </div>
                        <div class="panel-body imgPickerFondo" style="height: 200px;overflow: auto;">
                            <input type='file' id="imgInp" style="display: none;"/>
                            <img id="blah" style="height: 100%; width: 100%; object-fit: cover; display: none;"  />
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="overflow: auto;">
                            <i class="fa fa-files-o" aria-hidden="true"></i> Imagenes de detalles
                            <button class="btn btn-primary btn btn-primary btn-xs pull-right" v-on:click="openImagesSelectorMultiples()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar</button>
                        </div>
                        <div class="panel-body imgPickerFondo" style="height: 246px;overflow: auto;">
                            <a class="file_input" style="display: none;" data-jfiler-name="files" data-jfiler-extensions="jpg, jpeg, png, gif"><i class="icon-jfi-paperclip"></i> Attach a file</a>

                            <!--br>
                            <input type="submit"-->
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <select class="form-control">
                                <option>Selecciona el tipo</option>
                                <option>Residencial</option>
                                <option>Fraccionamiento</option>
                                <option>Edificio</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <input class="form-control" type="text" placeholder="Plantas">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <input class="form-control" type="text" placeholder="Construcción ( m2 )">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <input class="form-control" type="text" placeholder="Cuartos">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <input class="form-control" type="text" placeholder="Baños">
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-handshake-o" aria-hidden="true"></i> Servicios</div>
                        <div class="panel-body">
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Agua </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_luz" type="checkbox">
                                    <label for="chk_luz"> Luz </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_gas" type="checkbox">
                                    <label for="chk_gas"> Gas </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_internet" type="checkbox">
                                    <label for="chk_internet"> Internet </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-building-o" aria-hidden="true"></i> Extras</div>
                        <div class="panel-body">
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Alberca Privada </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Cochera </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Est. para Invitados </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Lavanderia </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Terraza </label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="chk_agua" type="checkbox">
                                    <label for="chk_agua"> Parque Privado </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!--Stylesheets-->
    <link href="{{ asset('css/libs/jquery.filer.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/libs/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/libs/jquery.filer.js') }}"></script>

    <script src="{{ asset('js/DesarrollosNuevo-dist.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk_HDgT86e3mHfgFSGPLO5T6L3bc2xx7Q&libraries=places&callback=map.initAutocomplete" async defer></script>

@stop