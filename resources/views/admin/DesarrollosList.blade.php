<a href="{{ url('parent/create') }}" style="margin-bottom: 10px;" class="btn btn-primary"><i class="fa fa-plus  " aria-hidden="true"></i> Nuevo Desarrollo</a>
<table id="myTable" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>Top</th>
      <th>( <i class="fa fa-building-o" aria-hidden="true"></i> ) Nombre del Desarrollo</th>
      <th>( <i class="fa fa-envelope" aria-hidden="true"></i> ) Correo</th>
      <th>( <i class="fa fa-battery-full" aria-hidden="true"></i> ) Estatus</th>
      <th>( <i class="fa fa-bars" aria-hidden="true"></i> ) Opciones</th>
    </tr>
  </thead>
  <tbody id="tableContentParents">

  </tbody>
</table>

<link href="{{ asset('css/datatable.css') }}" rel="stylesheet" />
<script src="{{ asset('js/datatable.js') }}"></script>
<script src="{{ asset('js/DesarrollosList-dist.js') }}"></script>