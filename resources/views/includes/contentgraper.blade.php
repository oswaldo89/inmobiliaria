
			<div class="container">
				<div class="row">

					<!-- BEGIN MAIN CONTENT -->
					<div class="main col-sm-8">
						<h1 class="section-title" data-animation-direction="from-bottom" data-animation-delay="50">Por que usar IDEALPLACE ?</h1>

						<div class="feature col-sm-4" data-animation-direction="from-bottom" data-animation-delay="250">
							<i class="fa fa-search "></i>
							<h3>Encuentra facil y rápido</h3>
							<p>Una base de datos centralizada que facilita la busqueda de inmuebles.</p>
							<!--a href="about.html" class="btn btn-default-color">Ver mas</a-->
						</div>
						<div class="feature col-sm-4" data-animation-direction="from-bottom" data-animation-delay="450">
							<i class="fa fa-home"></i>
							<h3>Variedad de inmuebles para escoger</h3>
							<p>Los mas importantes desarrollos del pais se encuentran aqui, busca tu mejor opción.</p>
							<!--a href="about.html" class="btn btn-default-color">Read More</a-->
						</div>
						<div class="feature col-sm-4" data-animation-direction="from-bottom" data-animation-delay="650">
							<i class="fa fa-envelope"></i>
							<h3>Recibe emails</h3>
							<p>Sucribete para recibir constantemente y antes que todos las nuevas publicaciones de los desarrollos.</p>
							<!--a href="about.html" class="btn btn-default-color">Read More</a-->
						</div>

					</div>
					<!-- END MAIN CONTENT -->

					<!-- BEGIN SIDEBAR -->
					<div class="sidebar colored col-sm-4">

						<!-- BEGIN TESTIMONIALS -->
						<div id="testimonials" class="col-sm-12" data-animation-direction="from-bottom" data-animation-delay="200">
							<h2 class="section-title">Testimonios</h2>

							<div id="testimonials-slider" class="owl-carousel testimonials">
								<div class="item">
									<blockquote class="text">
										<p>Desde que empeze a usar IDEALPLACE el numero de clientes que buscan nuestros desarrollos aumento.</p>
									</blockquote>
									<div class="author">
										<img src="http://placehold.it/61x61" alt="" />
										<div>
											Oswaldo Gomez<br>
											<span>Director General de Casas GEO.</span>
										</div>
									</div>
								</div>

								<div class="item">
									<blockquote class="text">
										<p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla risus at, imperdiet turpis.</p>
									</blockquote>
									<div class="author">
										<img src="http://placehold.it/61x61" alt="" />
										<div>
											John Doe<br>
											<span>CTO at Dolor Sit Amet Agency</span>
										</div>
									</div>
								</div>

								<div class="item">
									<blockquote class="text">
										<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla risus at, imperdiet turpis. Nullam ac enim auctor, fringilla risus at, imperdiet turpis.</p>
									</blockquote>
									<div class="author">
										<img src="http://placehold.it/61x61" alt="" />
										<div>
											Mary Smith<br>
											<span>UFO at Some Agency</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END TESTIMONIALS -->

					</div>
					<!-- END SIDEBAR -->

				</div>
			</div>
