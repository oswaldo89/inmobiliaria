<meta charset="utf-8" />
<title>Best Places</title>

<!-- meta tags -->
<meta name="keywords" content="real state, inmobiliarias, desarrollos, casas,realstate, venta casas, renta casas " />
<meta name="description" content="IdealPlace, plataforma unica para encontrar la vivienda de tus sueños." />
<meta name="author" content="Oswaldo Daniel Gomez Huerta" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<!-- estilos  -->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/currency-dist.js') }}"></script>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

