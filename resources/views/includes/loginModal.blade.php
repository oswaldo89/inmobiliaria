<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <button type="button" class="close btn-close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="loginmodal-container" id="loginApp">
            <form>
                <h1>Ingresa a tu cuenta</h1>
                <br>
                <input type="text" v-model="model.email" name="email" placeholder="correo">
                <input type="password" v-model="model.password" name="password" placeholder="contraseña">
                <input type="button" class="login loginmodal-submit" value="Entrar" v-on:click="signin()">
                <div class="login-help">
                    <a href="#">Olvide mi contraseña</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/Ingresar-dist.js') }}"></script>