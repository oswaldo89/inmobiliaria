<div id="nav-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{ url('/') }}" class="nav-logo"><img src="{{URL::to('/imgs/logo.png')}}" alt="Cozy Logo" /></a>
                <!-- BEGIN MAIN MENU -->
                <nav class="navbar">
                    <button id="nav-mobile-btn" class="visible-xs"><i class="fa fa-bars"></i></button>

                    <ul class="nav navbar-nav">
                        @if (Auth::guest())
                          <li><a href="#" data-toggle="modal" data-target="#login-modal" style="color: #df4a43;">Entrar</a></li>
                          <li><a href="#"  style="color: #000;">|</a></li>
                          <li><a href="{{ url('register') }}"  style="color: #df4a43;">Registrarme</a></li>
                        @else
                          <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"  style="color: #df4a43;">Salir</a></li>

                            @if (!Request::is('admin'))
                                <li><a href="#"  style="color: #000;">|</a></li>
                                <li><a href="{{ url('admin') }}" style="color: #df4a43;">Administrar</a></li>
                            @endif

                        @endif



                    </ul>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </nav>
                <!-- END MAIN MENU -->
            </div>
            @if (Auth::check())
              <div class="col-sm-12">
                <span style="position: absolute; right: 55px; bottom: 5px;">Bienvenido <strong>{{Auth::user()->ceoName}}</strong></span>
              </div>
            @endif
        </div>
    </div>
</div>