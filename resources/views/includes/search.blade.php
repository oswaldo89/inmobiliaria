
			<div class="container">
				<div class="row">
					<div class="col-sm-12" data-animation-direction="from-top" data-animation-delay="50">

						<h2 class="slider-title">Buscar la casa de tus sueños</h2>
						<div class="slider-subtitle">nunca fue tan fácil y rápido. Todos los desarrollos en un mismo lugar.</div>
					</div>

					<div id="home-search-buttons" class="col-sm-6 col-sm-offset-3" data-animation-direction="from-bottom" data-animation-delay="250">

						<div class="input-group">
							<input type="text" placeholder="Ciudad, Estado, Pais, etc..." name="home_search" id="home_search" class="form-control" />
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-search"></i>Buscar</button>
							</span>
						</div>
					</div>
				</div>
			</div>
