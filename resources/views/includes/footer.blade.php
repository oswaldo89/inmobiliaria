
			<div id="footer-top" class="container">
				<div class="row">
					<div class="block col-sm-3">
						<a href="index.html"><img src="{{URL::to('/imgs/logo.png')}}" alt="Cozy Logo" /></a>
						<br><br>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p>
					</div>
					<div class="block col-sm-3">
						<h3>Información de contacto</h3>
						<ul class="footer-contacts">
							<li><i class="fa fa-map-marker"></i> 24th Street, New York, USA</li>
							<li><i class="fa fa-phone"></i> 00351 123 456 789</li>
							<li><i class="fa fa-envelope"></i> <a href="mailto:email@yourbusiness.com">email@yourbusiness.com</a></li>
						</ul>
					</div>
					<div class="block col-sm-3">
						<h3>Helpful Links</h3>
						<ul class="footer-links">
							<li><a href="properties-list.html">All Properties Available</a></li>
							<li><a href="agent-listing.html">Look for an Agent</a></li>
							<li><a href="agency-listing.html">Look for an Agency</a></li>
							<li><a href="pricing-tables.html">See our Pricing Tables</a></li>
						</ul>
					</div>
					<div class="block col-sm-3">
						<h3>Latest Listings</h3>
						<ul class="footer-listings">
							<li>
								<div class="image">
									<a href="properties-detail.html"><img src="http://placehold.it/760x670" alt="" /></a>
								</div>
								<p><a href="properties-detail.html">Luxury Apartment with great views<span>+</span></a></p>
							</li>
							<li>
								<div class="image">
									<a href="properties-detail.html"><img src="http://placehold.it/760x670" alt="" /></a>
								</div>
								<p><a href="properties-detail.html">Stunning Villa with 5 bedrooms<span>+</span></a></p>
							</li>
							<li>
								<div class="image">
									<a href="properties-detail.html"><img src="http://placehold.it/760x670" alt="" /></a>
								</div>
								<p><a href="properties-detail.html">Recent construction with 3 bedrooms.<span>+</span></a></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
