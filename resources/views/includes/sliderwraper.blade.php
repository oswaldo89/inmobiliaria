
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="section-title" data-animation-direction="from-bottom" data-animation-delay="50">Nuevos desarrollos disponibles</h1>

						<div id="new-properties-slider" class="owl-carousel carousel-style1">

							<div class="item" data-animation-direction="from-bottom" data-animation-delay="250">
								<div class="image" style="height: 300px;">
									<a href="properties-detail.html" class="info">
										<h3>Hacienda las Fuentes</h3>
									</a>
									<img src="https://image.prntscr.com/image/_cjAm2IsRDCBA2Lk25JmmA.png" alt="" />
								</div>
								<div class="price">
									<i class="fa fa-home"></i>San Pedro NL
									<span>Casas GEO</span>
								</div>
							</div>

							<div class="item" data-animation-direction="from-bottom" data-animation-delay="450">
								<div class="image" style="height: 300px;">
									<a href="properties-detail.html" class="info">
										<h3>Stunning Villa with 5 bedrooms</h3>
										<span class="location">Miami Beach, Florida</span>
									</a>
									<img src="https://image.prntscr.com/image/qaKxjL6bQGeYHd3NWzNxiA.png" alt="" />
								</div>
								<div class="price">
									<i class="fa fa-home"></i>Santa catarina NL
									<span>TRAZZO</span>
								</div>
							</div>

							<div class="item" data-animation-direction="from-bottom" data-animation-delay="650">
								<div class="image">
									<a href="properties-detail.html" class="info">
										<h3>Cumbres San Agustin</h3>
									</a>
									<img src="https://image.prntscr.com/image/KYF7zeV_RCilI1h0v4QBmQ.png" alt="" />
								</div>
								<div class="price">
									<i class="fa fa-home"></i>Monterrey NL
									<span>GP</span>
								</div>
							</div>

							<div class="item" data-animation-direction="from-bottom" data-animation-delay="850">
								<div class="image">
									<a href="properties-detail.html" class="info">
										<h3>Modern construction with parking space</h3>
										<span class="location">Midtown, New York</span>
									</a>
									<img src="https://image.prntscr.com/image/wFPyFyOTSmi3PFuOQyRTKQ.png" alt="" />
								</div>
								<div class="price">
									<i class="fa fa-home"></i>Garcia NL
									<span>ARA</span>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
