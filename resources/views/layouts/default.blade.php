<!doctype html>
<html>

<head>
    @include('includes.head')
    <!-- INCLUDES/SCRIPTS -->
    @include('includes.scripts')
    <!-- INCLUDES/SCRIPTS -->
</head>

<script>
    var BASE_PATH = "{{URL::to('/')}}";
    var ADMIN_PATH = "{{URL::to('/admin')}}";

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>

<body>
    <!-- INCLUDES/HEADER -->
    <header id="header">
        @include('includes.header')
    </header>
    <!-- INCLUDES/HEADER -->

    <div id="main" class="row">
        @yield('content')
    </div>

    <!-- INCLUDES/FOOTER -->
    <footer id="footer">
        @include('includes.footer')
    </footer>
    <!-- INCLUDES/FOOTER -->
    
    @include('includes.loginModal')
    
</body>

</html>