Vue.use(VueMask.VueMaskPlugin);
var selectCountry = {id_pais: '0', nombre: 'Pais *'};
var selectState = {cve_ent: '0', nom_ent: 'Estado *'};
var selectCity = {cve_mun: '0', nom_mun: 'Ciudad *'};
var imgBase64 = "";
var geoLocationMap = "";
var map = {
    markers: [],
    map: null,
    initAutocomplete: function () {
        var self = this;
        self.map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 24.258498, lng: -102.645287
            },
            zoom: 6,
            mapTypeId: 'roadmap'
        });

        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        self.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        self.map.addListener('bounds_changed', function () {
            searchBox.setBounds(self.map.getBounds());
        });

        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            self.map.fitBounds(bounds);
        });

        google.maps.event.addListener(self.map, 'click', function (event) {
            self.deleteMarkers();
            self.addMarker(event.latLng, self.map);
        });
    },
    addMarker: function (location) {
        geoLocationMap = location.lat() + "," + location.lng();
        var self = this;
        var marker = new google.maps.Marker({
            position: location,
            map: self.map
        });
        self.markers.push(marker);
    },
    deleteMarkers: function () {
        var self = this;
        self.clearMarkers();
        self.markers = [];
    },
    clearMarkers: function () {
        var self = this;
        self.setMapOnAll(null);
    },
    setMapOnAll: function (map) {
        var self = this;
        for (var i = 0; i < self.markers.length; i++) {
            self.markers[i].setMap(map);
        }
    }
};

var app = new Vue({
    el: '#app',
    data: {
        model: {
            name: '',
            telephone: '',
            email: '',
            img: '',
            paisId: '0',
            estadoId: '0',
            municipioId: '0',
            geolocation: ''
        },
        disabledFormSubmit : false,
        country: '0',
        countries: [selectCountry],
        state: '0',
        states: [selectState],
        city: '0',
        cities: [selectCity]
    },
    mounted: function () {
        this.getCountries();
    },
    watch: {
        country: function (val, oldVal) {
            this.resetStates();
            this.resetCities();
            this.getStates(val);
        },
        state: function (val, oldVal) {
            this.resetCities();
            this.getCities(val);
        }
    },
    methods: {
        save: function () {
            var self = this;
            self.disableForm(true);

            self.model.paisId = this.country;
            self.model.estadoId = this.state;
            self.model.municipioId = this.city;
            self.model.img = imgBase64;
            self.model.geolocation = geoLocationMap;

            if (self.isAnyEmptyProperty()) {
                swal('Oops...', 'Todos los campos con (*) son requeridos.', 'error');
                self.disableForm(false);
                return false;
            }

            axios.post('/api/parent', self.model).then(function (response) {
                if (response.data.status) {
                    swal('Buen trabajo!', response.data.message, 'success').then(function (res) {
                        window.location = ADMIN_PATH;
                    }, function (dismiss) {
                        window.location = ADMIN_PATH;
                    });
                } else {
                    self.disableForm(false);
                    swal('Oops...', response.data.message, 'error');
                }
            }).catch(function (error) {
                self.disableForm(false);
            });
        },
        getCountries: function () {
            var self = this;
            axios.get('/api/country', self.model).then(function (response) {
                self.countries = self.countries.concat(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        getStates: function (id_pais) {
            var self = this;
            axios.get('/api/state/' + id_pais).then(function (response) {
                self.states = self.states.concat(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        getCities: function (id_state) {
            var self = this;
            axios.get('/api/city/' + id_state).then(function (response) {
                self.cities = self.cities.concat(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        resetStates: function () {
            this.states = [];
            this.states.push(selectState);
            this.state = '0';
        },
        resetCities: function () {
            this.cities = [];
            this.cities.push(selectCity);
            this.city = '0';
        },
        openImagesSelector: function () {
            $("#imgInp").click();
        },
        openImagesSelectorMultiples: function () {
            $(".file_input").click();
        },
        disableForm: function (value) {
            this.disabledFormSubmit = value;
            if (value) {
                NProgress.start();
            } else {
                NProgress.done();
            }
        },
        isAnyEmptyProperty: function () {
            return !(this.model.name !== "" && this.model.telephone !== "" &&
                this.model.email !== "" && this.model.geolocation !== "");
        }
    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            imgBase64 = e.target.result;
            $('#blah').attr('src', e.target.result);
            $("#blah").show();
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    readURL(this);
});
