var table;
var vueList = new Vue({
    el: '#tab_default_1',
    data: {
        model: {
            parentData: []
        }
    },
    mounted: function () {
        this.getParentNodes();
    },
    methods: {
        getParentNodes: function () {
            var self = this;
            var body = "";
            $('#myTable tbody > tr').remove();
            axios.get('/api/parent').then(function (response) {

                response.data.forEach(function (item, index) {
                    index++;
                    body = "<tr>";
                    body += "<td>" + index + "</td>";
                    body += "<td>" + item.name.toUpperCase() + "</td>";
                    body += "<td>" + item.email.toUpperCase() + "</td>";
                    body += "<td style=\"text-align:center;\">" + (item.isActive ? 'ACTIVO' : 'INACTIVO') + "</td>";
                    body += "<td style=\"text-align:center;\">" +
                        "<button type=\"button\" class=\"btn-primary btn-sm btnEdit\" idParentToEdit=" + item.id + "><i class=\"fa fa-pencil-square-o\"></i></button>" +
                        "<button type=\"button\" class=\"btn-primary btn-sm btnDelete\" idParent=" + item.id + "><i class=\"fa fa-trash-o\" ></i></button>" +
                        "<button type=\"button\" class=\"btn-primary btn-sm\"><i class=\"fa fa-envelope-o\"></i></button>" +
                        "</td>";

                    body += "</tr>";

                    $("#myTable").find("tbody").append(body);
                });
                self.initDataTable();
            }).catch(function (error) {
                console.log(error);
            });
        },
        initDataTable: function () {
            var table = $('#myTable').dataTable({
                "bRetrieve": true,
                "autoWidth": false,
                "iDisplayLength": 5,
                "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });

        }
    }
});


$(document).ready(function () {
    $(document).on("click", ".btnDelete", function () {
        var id = $(this).attr('idParent');
        swal({
            title: 'Esta seguro de eliminar el registro?',
            text: "No podra revertir esta operacion, desea continuar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!'
        }).then(function () {

            axios.delete('/api/parent/' + id).then(function (response) {
                swal(
                    response.data.status ? 'Eliminado!' : 'Error',
                    response.data.message,
                    'success'
                );
                vueList.getParentNodes();
            });

        })
    });

    $(document).on("click", ".btnEdit", function () {
        var id = $(this).attr('idParentToEdit');
        window.location.href = BASE_PATH + "/parent/" + id + "/edit";
    });


});