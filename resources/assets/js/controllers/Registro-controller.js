Vue.use(VueMask.VueMaskPlugin);
/* vue code */
var selectCountry = { id_pais:'0' , nombre: 'Selecciona un pais *' };
var selectState = { cve_ent:'0' , nom_ent: 'Selecciona un estado *' };
var selectCity = { cve_mun:'0' , nom_mun: 'Selecciona una ciudad *' };

// noinspection SpellCheckingInspection
var vue = new Vue({
    el: '#register',
    data: {
        model: {
            logo: '',
            telephone: '',
            address: '',
            paisId: '0',
            estadoId: '0',
            municipioId: '0',
            companyName: '',
            ceoName: '',
            email: '',
            password: '',
            secondPassword: ''
        },
        disabledFormSubmit : false,
        country: '0',
        countries: [selectCountry],
        state: '0',
        states: [selectState],
        city: '0',
        cities: [selectCity]
    },
    mounted: function(){
      this.getCountries();
    },
    watch:{
      country: function(val, oldVal){
        this.resetStates();
        this.resetCities();
        this.getStates(val);
      },
      state: function(val, oldVal){
        this.resetCities();
        this.getCities(val);
      }
    },
    methods: {
        createAccount: function() {
          
          var self = this;
          
          self.disableForm(true);
          self.model.logo = $('#image-cropper').cropit('export');

          if (self.isAnyEmptyProperty()) {
              swal('Oops...', 'Todos los campos con (*) son requeridos.', 'error');
              self.disableForm(false);
              return false;
          }
          if(!validateEmail(self.model.email)){
              swal('Error', 'Ingresa un correo con formato valido', 'error');
              self.disableForm(false);
              return false;
          }
          
          if(self.model.password !== self.model.secondPassword){
              swal('Error', 'La contraseña no coinside.', 'error');
              self.disableForm(false);
              return false;
          }
          self.model.paisId = this.country;
          self.model.estadoId = this.state;
          self.model.municipioId = this.city;
          
          axios.post('/api/user', self.model).then(function(response) {
          
              self.disableForm(false);
              if(response.data.status){
                swal('Buen trabajo!', 'Se te ha enviado un correo para validar la cuenta.!', 'success').then(function(res) {
                    window.location = BASE_PATH;
                },function (dismiss) {
                    window.location = BASE_PATH;
                });
              }else{
                self.disableForm(false);
                swal('Oops...', response.data.message , 'error');
              }
          }).catch(function(error) {
              self.disableForm(false);
          });
        },
        getCountries: function(){
          var self = this;
          axios.get('/api/country', self.model).then(function(response) {
              self.countries = self.countries.concat(response.data);
          }).catch(function(error) {
              console.log(error);
          });
        },
        getStates: function(id_pais){
          var self = this;
          axios.get('/api/state/'+id_pais).then(function(response) {
              self.states = self.states.concat(response.data);
          }).catch(function(error) {
              console.log(error);
          });
        },
        getCities: function(id_state){
          var self = this;
          axios.get('/api/city/'+id_state).then(function(response) {
              self.cities = self.cities.concat(response.data);
          }).catch(function(error) {
              console.log(error);
          });
        },
        resetStates: function(){
          this.states = [];
          this.states.push(selectState);
          this.state = '0';
        },
        resetCities: function(){
          this.cities = [];
          this.cities.push(selectCity);
          this.city = '0';
        },
        selectImage: function(){
          this.$refs.inputImageFile.click();
        },
        disableForm: function(value){
          this.disabledFormSubmit = value;
          if(value){
            NProgress.start();
          }else{
            NProgress.done();
          }
        },
        isAnyEmptyProperty: function() {
            return !(this.model.companyName !== "" && this.model.ceoName !== "" &&
            this.model.email !== "" && this.model.password !== "" &&
            this.model.secondPassword !== "" && this.country !== 0 &&
            this.state !== 0 && this.city !== 0);
        }
    }
});

/* jquery code */
$('#image-cropper').cropit();