<?php

//Rutas de Registro
Route::get('/', function() { return View::make('pages.home'); });
Route::get('/register', function(){ return View::make('pages.register'); });
Route::get('/resetPassword', function(){ return View::make('pages.resetPassword'); });
Route::get('/verification-response', function(){ return View::make('pages.confirmation_code'); });
Route::resource('code', 'VerificationController');

//Rutas para iniciar sesion
Route::post('login', [ 'as' => '', 'uses' => 'Auth\LoginController@login' ]);
Route::post('logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout' ]);

//Administrador
Route::resource('admin', 'AdministrationController');

//Agregar Nuevo Desarrollo
Route::resource('parent', 'ParentNodeController');
